package com.zavod.redditscraper.scraperservice.task;

import com.zavod.redditscraper.scraperservice.service.SubmissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ScheduledScraperTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledScraperTask.class);

    private Map<String, String> subredditsWithBeforeParam;

    @Autowired
    private Environment environment;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BuildProperties buildProperties;

    @Autowired
    private SubmissionService submissionService;

    @Scheduled(fixedRateString = "${scraper.fixedRate}", initialDelay = 5000)
    public void scheduleTaskWithInitialDelay() {
        if (subredditsWithBeforeParam == null) {
            LOGGER.info("Loading subreddits from config file application-docker.yml...");

            subredditsWithBeforeParam = getSubredditsWithBeforeParamMap();

            LOGGER.info("Loaded subreddits=<{}>", subredditsWithBeforeParam.keySet());
        }

        if (subredditsWithBeforeParam.size() == 0){
            throw new RuntimeException("No subreddits configured in application-docker.yml file");
        }

        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int poolSize = subredditsWithBeforeParam.size() > availableProcessors ?
                availableProcessors : subredditsWithBeforeParam.size();

        LOGGER.info("Initialize scheduled executor pool for subreddits polling tasks...");
        ExecutorService executor = Executors.newFixedThreadPool(poolSize);
        for (String subreddit : subredditsWithBeforeParam.keySet()){

            LOGGER.info("Submit task for polling subreddit=<{}>", subreddit);
            executor.execute(new ScraperTask(
                    restTemplate,
                    buildProperties,
                    submissionService,
                    subredditsWithBeforeParam,
                    subreddit));
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        LOGGER.info("Scheduled executor pool finished all polling tasks");
    }

    private Map<String, String> getSubredditsWithBeforeParamMap(){
        Map<String, String> map = new ConcurrentHashMap<>();
        String[] subreddits = environment.getProperty("reddit.subreddits", "").split(", ");

        for (int i=0; i<subreddits.length; i++){
            map.put(subreddits[i], "");
        }

        return map;
    }
}

