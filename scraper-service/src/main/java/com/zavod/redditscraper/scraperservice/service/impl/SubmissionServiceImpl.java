package com.zavod.redditscraper.scraperservice.service.impl;

import com.zavod.redditscraper.commons.entity.RedditEntity;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;
import com.zavod.redditscraper.scraperservice.model.comment.RedditCommentsResponse;
import com.zavod.redditscraper.scraperservice.model.submission.Submission;
import com.zavod.redditscraper.commons.repository.RedditEntityRepository;
import com.zavod.redditscraper.scraperservice.service.CommentService;
import com.zavod.redditscraper.scraperservice.service.SubmissionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubmissionServiceImpl.class);

    private static final String PLATFORM = System.getProperty("os.name");
    private static final String BASE_URL = "https://www.reddit.com";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BuildProperties buildProperties;

    @Autowired
    private CommentService commentService;

    @Autowired
    private RedditEntityRepository redditEntityRepository;

    @Override
    public String processRedditSubmissions(List<RedditDataChildren<Submission>> submissionList) {
        LOGGER.info("Processing submission list with <{}> entries...", submissionList.size());

        for (RedditDataChildren<Submission> submissionChildren : submissionList){
            Submission submission = submissionChildren.getData();
            if (submission != null){
                RedditEntity redditEntity = new RedditEntity();
                redditEntity.setSubreddit(submission.getSubreddit());
                redditEntity.setCreated(submission.getCreated());
                redditEntity.setText(submission.getTitle());
                redditEntityRepository.save(redditEntity);

                if (StringUtils.isNotEmpty(submission.getPermalink())){
                    processSubmissionComments(submission.getPermalink());
                }

            }
        }

        return !submissionList.isEmpty() && submissionList.get(0).getData() != null ?
                submissionList.get(0).getData().getName()
                : null;
    }

    private void processSubmissionComments(String url){
        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", getUserAgent());
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<RedditCommentsResponse>> response;
        try {
            String commentsUrl = BASE_URL + url + ".json";

            LOGGER.info("Get comments form url=<{}>", commentsUrl);
            response = restTemplate.exchange(
                    commentsUrl,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<List<RedditCommentsResponse>>() {});

            if (HttpStatus.OK.equals(response.getStatusCode())){
                LOGGER.info("Received an OK resposne from url=<{}>", commentsUrl);
                commentService.processRedditComments(response.getBody());
            } else {
                LOGGER.error("{} response status code received when polling for submission comments from url=<{}>",
                        response.getStatusCode(), url);
            }
        } catch (RestClientException e){
            LOGGER.error("RestClientException when polling for submission comments from url=<{}>", url);
        }
    }

    private String getUserAgent() {
        return PLATFORM +
                ":" + buildProperties.getGroup() + "." + buildProperties.getArtifact() +
                ":" + buildProperties.getVersion();
    }
}
