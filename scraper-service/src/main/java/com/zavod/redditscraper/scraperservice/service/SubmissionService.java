package com.zavod.redditscraper.scraperservice.service;

import com.zavod.redditscraper.scraperservice.model.submission.Submission;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;

import java.util.List;

public interface SubmissionService {

    String processRedditSubmissions(List<RedditDataChildren<Submission>> submissionList);
}
