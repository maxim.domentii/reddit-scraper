package com.zavod.redditscraper.scraperservice.model.comment;

public class Comment {

    private String id;
    private String subreddit;
    private String body;
    private long created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id='" + id + '\'' +
                ", subreddit='" + subreddit + '\'' +
                ", body='" + body + '\'' +
                ", created=" + created +
                '}';
    }
}
