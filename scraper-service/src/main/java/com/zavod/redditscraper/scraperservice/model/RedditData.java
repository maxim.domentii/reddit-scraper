package com.zavod.redditscraper.scraperservice.model;

import java.util.List;

public class RedditData<T> {

    private int dist;
    private List<RedditDataChildren<T>> children;
    private String after;
    private String before;

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public List<RedditDataChildren<T>> getChildren() {
        return children;
    }

    public void setChildren(List<RedditDataChildren<T>> children) {
        this.children = children;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }
}
