package com.zavod.redditscraper.scraperservice.model.comment;

import com.zavod.redditscraper.scraperservice.model.RedditData;

public class RedditCommentsResponse {

    private String kind;
    private RedditData<Comment> data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public RedditData<Comment> getData() {
        return data;
    }

    public void setData(RedditData<Comment> data) {
        this.data = data;
    }
}
