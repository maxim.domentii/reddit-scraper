package com.zavod.redditscraper.scraperservice.task;

import com.zavod.redditscraper.scraperservice.model.submission.RedditSubmissionsResponse;
import com.zavod.redditscraper.scraperservice.service.SubmissionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class ScraperTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScraperTask.class);

    private static final String PLATFORM = System.getProperty("os.name");
    private static final String BASE_URL = "https://www.reddit.com/r/";

    private RestTemplate restTemplate;
    private BuildProperties buildProperties;
    private SubmissionService submissionService;

    private String subreddit;
    private Map<String, String> subredditsWithBeforeParam;

    public ScraperTask(RestTemplate restTemplate,
                       BuildProperties buildProperties,
                       SubmissionService submissionService,
                       Map<String, String> subredditsWithBeforeParam,
                       String subreddit){
        this.restTemplate = restTemplate;
        this.buildProperties = buildProperties;
        this.submissionService = submissionService;
        this.subredditsWithBeforeParam = subredditsWithBeforeParam;
        this.subreddit = subreddit;
    }

    @Override
    public void run() {

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", getUserAgent());
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<RedditSubmissionsResponse> response;
        try {
            String beforeParam = StringUtils.isNotEmpty(subredditsWithBeforeParam.get(subreddit)) ?
                    subredditsWithBeforeParam.get(subreddit)
                    : "";
            String url = BASE_URL + subreddit + "/new.json?before=" + beforeParam;

            LOGGER.info("Get submissions from url=<{}>", url);
            response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    entity,
                    RedditSubmissionsResponse.class);

            if (HttpStatus.OK.equals(response.getStatusCode())){
                LOGGER.info("Received an OK resposne from url=<{}>", url);
                processResponse(response);
            } else {
                LOGGER.error("{} response status code received when polling for subreddit=<{}>",
                        response.getStatusCode(), subreddit);
            }
        } catch (RestClientException e){
            LOGGER.error("RestClientException when polling for the subreddit=<{}>", subreddit);
        }
    }

    private void processResponse(ResponseEntity<RedditSubmissionsResponse> response) {
        RedditSubmissionsResponse redditSubmissionsResponse = response.getBody();
        if (redditSubmissionsResponse != null
                && redditSubmissionsResponse.getData() != null
                && redditSubmissionsResponse.getData().getChildren() != null
                && !redditSubmissionsResponse.getData().getChildren().isEmpty()){

            String beforeParam = submissionService.processRedditSubmissions(
                    redditSubmissionsResponse.getData().getChildren());
            subredditsWithBeforeParam.put(subreddit, beforeParam);
        } else {
            LOGGER.info("No new submissions to be processed form last polling.");
        }
    }

    private String getUserAgent() {
        return PLATFORM +
                ":" + buildProperties.getGroup() + "." + buildProperties.getArtifact() +
                ":" + buildProperties.getVersion();
    }
}
