package com.zavod.redditscraper.scraperservice.model.submission;

import com.zavod.redditscraper.scraperservice.model.RedditData;

public class RedditSubmissionsResponse {

    private String kind;
    private RedditData<Submission> data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public RedditData<Submission> getData() {
        return data;
    }

    public void setData(RedditData<Submission> data) {
        this.data = data;
    }
}
