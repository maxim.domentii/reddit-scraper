package com.zavod.redditscraper.scraperservice.service;

import com.zavod.redditscraper.scraperservice.model.comment.RedditCommentsResponse;

import java.util.List;

public interface CommentService {

    void processRedditComments(List<RedditCommentsResponse> redditComments);
}
