package com.zavod.redditscraper.scraperservice.service.impl;

import com.zavod.redditscraper.commons.entity.RedditEntity;
import com.zavod.redditscraper.scraperservice.model.comment.Comment;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;
import com.zavod.redditscraper.scraperservice.model.comment.RedditCommentsResponse;
import com.zavod.redditscraper.commons.repository.RedditEntityRepository;
import com.zavod.redditscraper.scraperservice.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentServiceImpl.class);

    @Autowired
    private RedditEntityRepository redditEntityRepository;

    @Override
    public void processRedditComments(List<RedditCommentsResponse> redditComments) {
        if (redditComments != null && redditComments.size() == 2
                && redditComments.get(1).getData() != null
                && redditComments.get(1).getData().getChildren() != null){

            List<RedditDataChildren<Comment>> commentList = redditComments.get(1).getData().getChildren();
            LOGGER.info("No comments to be processed form last polling.");

            LOGGER.info("Processing comment list with <{}> entries...", commentList.size());

            for (RedditDataChildren<Comment> commentChildren : commentList){
                Comment comment = commentChildren.getData();
                if (comment != null){
                    RedditEntity redditEntity = new RedditEntity();
                    redditEntity.setSubreddit(comment.getSubreddit());
                    redditEntity.setCreated(comment.getCreated());
                    redditEntity.setText(comment.getBody());
                    redditEntityRepository.save(redditEntity);
                }

            }
        }
    }
}
