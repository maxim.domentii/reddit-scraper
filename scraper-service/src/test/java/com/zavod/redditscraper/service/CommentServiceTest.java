package com.zavod.redditscraper.service;

import com.zavod.redditscraper.commons.repository.RedditEntityRepository;
import com.zavod.redditscraper.scraperservice.model.RedditData;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;
import com.zavod.redditscraper.scraperservice.model.comment.Comment;
import com.zavod.redditscraper.scraperservice.model.comment.RedditCommentsResponse;
import com.zavod.redditscraper.scraperservice.service.CommentService;
import com.zavod.redditscraper.scraperservice.service.impl.CommentServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CommentServiceTest {

    @TestConfiguration
    static class CommentServiceTestContextConfiguration {

        @Bean
        public CommentService commentService() {
            return new CommentServiceImpl();
        }
    }

    @Autowired
    private CommentService commentService;

    @MockBean
    private RedditEntityRepository redditEntityRepository;

    @Test
    public void whenProcessRedditCommentsForEmptyListOfComments_thenNoNewEntrySaved(){
        List<RedditCommentsResponse> redditComments = new ArrayList<>();
        RedditCommentsResponse redditCommentsResponse0  = new RedditCommentsResponse();
        redditComments.add(redditCommentsResponse0);
        RedditCommentsResponse redditCommentsResponse1 = new RedditCommentsResponse();
        RedditData<Comment> redditData = new RedditData<>();
        redditData.setChildren(Lists.emptyList());
        redditCommentsResponse1.setData(redditData);
        redditComments.add(redditCommentsResponse1);

        commentService.processRedditComments(redditComments);

        verifyZeroInteractions(redditEntityRepository);
    }

    @Test
    public void whenProcessRedditComments_thenCallRepositorySave(){
        List<RedditCommentsResponse> redditComments = new ArrayList<>();
        RedditCommentsResponse redditCommentsResponse0  = new RedditCommentsResponse();
        redditComments.add(redditCommentsResponse0);
        RedditCommentsResponse redditCommentsResponse1 = new RedditCommentsResponse();
        RedditData<Comment> redditData = new RedditData<>();
        RedditDataChildren<Comment> dataChildren = new RedditDataChildren<>();
        Comment comment = new Comment();
        dataChildren.setData(comment);
        redditData.setChildren(Lists.list(dataChildren));
        redditCommentsResponse1.setData(redditData);
        redditComments.add(redditCommentsResponse1);

        commentService.processRedditComments(redditComments);

        verify(redditEntityRepository, times(1)).save(any());
    }

}
