package com.zavod.redditscraper.service;

import com.zavod.redditscraper.commons.repository.RedditEntityRepository;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;
import com.zavod.redditscraper.scraperservice.model.comment.RedditCommentsResponse;
import com.zavod.redditscraper.scraperservice.model.submission.Submission;
import com.zavod.redditscraper.scraperservice.service.CommentService;
import com.zavod.redditscraper.scraperservice.service.SubmissionService;
import com.zavod.redditscraper.scraperservice.service.impl.SubmissionServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class SubmissionServiceTest {

    @TestConfiguration
    static class SubmissionServiceTestContextConfiguration {

        @Bean
        public SubmissionService submissionService() {
            return new SubmissionServiceImpl();
        }
    }

    @Autowired
    private SubmissionService submissionService;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private RedditEntityRepository redditEntityRepository;

    @MockBean
    private BuildProperties buildProperties;

    @MockBean
    private CommentService commentService;

    @Test
    public void whenProcessRedditSubmissionsForEmptyList_thenNoNewEntrySaved(){
        submissionService.processRedditSubmissions(Lists.newArrayList());

        verifyZeroInteractions(redditEntityRepository);
        verifyZeroInteractions(restTemplate);
    }

    @Test
    public void whenProcessRedditSubmissionsWithoutPermlink_thenCallRepositorySaveWithoutPollingForComments(){
        Submission submission = new Submission();
        RedditDataChildren<Submission> dataChildren = new RedditDataChildren<>();
        dataChildren.setData(submission);
        List<RedditDataChildren<Submission>> submissionList = Lists.list(dataChildren);

        submissionService.processRedditSubmissions(submissionList);

        verify(redditEntityRepository, times(1)).save(any());
        verifyZeroInteractions(restTemplate);
    }

    @Test
    public void whenProcessRedditSubmissionsWithoutPermlink_thenCallRepositorySaveAndPollForComments(){
        Submission submission = new Submission();
        submission.setPermalink("/r/java/comments/bejlvj/coming_from_c_i_have_some_problems_wrapping_my");
        RedditDataChildren<Submission> dataChildren = new RedditDataChildren<>();
        dataChildren.setData(submission);
        List<RedditDataChildren<Submission>> submissionList = Lists.list(dataChildren);

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", System.getProperty("os.name") + ":null.null:null");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        String url = "https://www.reddit.com/r/java/comments/bejlvj/coming_from_c_i_have_some_problems_wrapping_my.json";
        when(restTemplate.exchange(
                url,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<RedditCommentsResponse>>() {}))
                .thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));

        submissionService.processRedditSubmissions(submissionList);

        verify(redditEntityRepository, times(1)).save(any());
        verify(restTemplate, times(1))
                .exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<RedditCommentsResponse>>() {});
    }
}
