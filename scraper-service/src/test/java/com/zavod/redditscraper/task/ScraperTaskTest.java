package com.zavod.redditscraper.task;

import com.zavod.redditscraper.scraperservice.model.RedditData;
import com.zavod.redditscraper.scraperservice.model.RedditDataChildren;
import com.zavod.redditscraper.scraperservice.model.submission.RedditSubmissionsResponse;
import com.zavod.redditscraper.scraperservice.model.submission.Submission;
import com.zavod.redditscraper.scraperservice.service.SubmissionService;
import com.zavod.redditscraper.scraperservice.task.ScraperTask;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class ScraperTaskTest {

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private BuildProperties buildProperties;

    @MockBean
    private SubmissionService submissionService;

    private ScraperTask scraperTask;

    @Before
    public void init(){
        scraperTask = new ScraperTask(
                restTemplate, buildProperties, submissionService, new HashMap<>(), "subreddit");
    }

    @Test
    public void whenRunScraperTaskAndRestClientException_thenNoSubmissionServiceCalled(){
        when(restTemplate.exchange(anyString(), any(), any(), any(Class.class))).thenThrow(RestClientException.class);

        new Thread(scraperTask).start();

        verify(submissionService, times(0)).processRedditSubmissions(anyList());
    }

    @Test
    public void whenRunScraperTaskAndErrorCodeOnResposne_thenNoSubmissionServiceCalled(){
        ResponseEntity response = new ResponseEntity(HttpStatus.NOT_FOUND);
        when(restTemplate.exchange(anyString(), any(), any(), any(Class.class))).thenReturn(response);

        new Thread(scraperTask).start();

        verify(submissionService, times(0)).processRedditSubmissions(anyList());
    }

    @Test
    public void whenRunScraperTaskAnd200OnResponse_thenSubmissionServiceCalled() throws InterruptedException {
        RedditSubmissionsResponse body = new RedditSubmissionsResponse();
        RedditData<Submission> redditData = new RedditData<>();
        RedditDataChildren<Submission> dataChildren = new RedditDataChildren<>();
        redditData.setChildren(Lists.list(dataChildren));
        body.setData(redditData);
        ResponseEntity<RedditSubmissionsResponse> response = new ResponseEntity<RedditSubmissionsResponse>(
                body,
                HttpStatus.OK
        );

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", System.getProperty("os.name") + ":null.null:null");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        when(restTemplate.exchange(
                "https://www.reddit.com/r/subreddit/new.json?before=",
                HttpMethod.GET,
                entity,
                RedditSubmissionsResponse.class)).thenReturn(response);

        Thread t = new Thread(scraperTask);
        t.start();
        t.join();

        verify(submissionService, times(1)).processRedditSubmissions(Lists.list(dataChildren));
    }
}
