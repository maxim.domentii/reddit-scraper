# Reddit Scraper

Small system that parses live data, stores it in a database and exposes it with filtering capabilities 
through an HTTP API. The ingested data will consist of reddit submissions/comments.

## Prerequisite

- Java 8 or later
- Maven
- Docker
- docker-compose

## Build and run

1. cd path_to_project
2. mvn clean install
3. docker-compose up
   
   <b>Note:</b> After all containers are up you can access the Scraper API at localhost:8080 with request like
    
    - localhost:8080/items/?subreddit=subreddit&from=t1&to=t2 
    
    or
    
    - localhost:8080/items/?subreddit=subreddit&from=t1&to=t2&keyword=kw
    
    where t1 and t2 are in UNIX timestamp format.
    
## Use case

The idea is to monitor a set of subreddits, extracting the newest submissions and
comments. Using reddit’s API, system should fetch that data periodically, normalize the
data and store it in MongoDB in a standard format.
The way data is stored should allow to perform queries to retrieve items in a certain 
time frame. Also, as a deep-dive capability, system should allow to search for all
data containing a certain keyword, combined with the time frame.

<b>Input:</b> A list of subreddits, that should be passed through a config file (.json or .yml)

<b>Deliverables:</b>
1. A running MongoDB 3.0 database with minimal settings.
2. A long-running script that retrieves new submissions/comments in the
targeted subreddits, grabs them, formats them and writes them to mongo.
3. A web server that exposes the following endpoints for GET:
    
    /items/?subreddit=subreddit&from=t1&to=t2

    /items//?subreddit=subreddit&from=t1&to=t2&keyword=kw

    where:

    - subreddit is the subreddit name (e.g. for https://www.reddit.com/r/python
    the subreddit is python)
    - from and to are UNIX timestamps that define the time frame range.
    - the keyword parameter should restrict the returned items to only those containing
    the given keyword. For submissions, the keyword should match the title, whereas
    for comments the keyword should match the text. The keyword parameter is optional.

4. A minimal test suite that tests the functionality in the previous stages
5.  Dockerfile for each process and a Docker Compose file to join all containers.
In total, there will be 3 Docker containers, one for each of the following:
MongoDB, long-running script, web server.


<b>Notes</b>
- For a submission the mandatory fields are a unique id and the title. For comments,
  mandatory fields are a unique id and the comment text.
- Even though in reddit comments are connected to submissions (each comment
belongs to a submission), in this project look at each one as “just another item”, 
completely independent from each other without preserving any relationship between them 
in the database.
- Don’t fetch older data, the moment script is started is considered a starting point
and collect submissions and comments posted after that.
- The database doesn’t need to be emptied when running the script.
- The long-running script doesn’t have to be fault tolerant: if the script is stopped,
it has not to start collecting data from where it left off. Just start again fresh.
- Data returned by the web server should be in JSON format, sorted in reverse
chronological order. For simplicity, all parameters are mandatory, except keyword, 
the server should respond with an error if they’re missing.

## Implementation

I have decided to implement this project with Java 8, Spring Boot and Spring Data MongoDB.

For the long running script, subreddits are configured in application.yml config file. I use 
Spring Scheduling to start an executor pool in which I submit one thread for each subreddit to 
parallelize the polling task. First polling is done for the hottest submissions and corresponding 
comments. After that script polls only for the submissions added after last one.

The functionality of the script is tested using unit test from corresponding module package.

The web server is a Spring boot app where I expose required endpoint with a Spring RestController.
The keyword search is performed on a Text Indexed field and result set is sorted in reverse
chronological order.   