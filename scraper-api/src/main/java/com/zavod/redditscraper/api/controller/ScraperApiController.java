package com.zavod.redditscraper.api.controller;

import com.zavod.redditscraper.commons.entity.RedditEntity;
import com.zavod.redditscraper.commons.repository.RedditEntityRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/")
public class ScraperApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScraperApiController.class);

    private static final Sort SORT_BY_CREATED_DESC = new Sort(Sort.Direction.DESC, "created");

    @Autowired
    private RedditEntityRepository redditEntityRepository;

    @GetMapping("/itmes")
    private List<RedditEntity> getItems(@RequestParam("subreddit") String subreddit,
                                        @RequestParam("from") Long from,
                                        @RequestParam("to") Long to,
                                        @RequestParam(value = "keyword", required = false) String keyword){

        LOGGER.info("Received an getItems request for subreddit=<{}>, from=<{}>, to=<{}> and keyword=<{}>",
                subreddit, from, to, keyword);

        List<RedditEntity> redditEntities;
        if (StringUtils.isEmpty(keyword)){
            redditEntities = redditEntityRepository.findBySubredditAndCreatedWithinSortByCreatedDesc(
                    subreddit, from, to, SORT_BY_CREATED_DESC);
        } else {
            redditEntities = redditEntityRepository.findBySubredditAndCreatedWithinSortByCreatedDesc(
                    subreddit, from, to, keyword, SORT_BY_CREATED_DESC);
        }

        return redditEntities;
    }
}
