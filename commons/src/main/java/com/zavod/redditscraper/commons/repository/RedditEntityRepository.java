package com.zavod.redditscraper.commons.repository;

import com.zavod.redditscraper.commons.entity.RedditEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RedditEntityRepository extends MongoRepository<RedditEntity, String> {

    @Query(value = "{ 'subreddit' : ?0, 'created' : {$gte : ?1, $lte: ?2 }}")
    List<RedditEntity> findBySubredditAndCreatedWithinSortByCreatedDesc(
            String subreddit, long from, long to, Sort sort);

    @Query(value = "{ 'subreddit' : ?0, 'created' : { $gte : ?1, $lte: ?2 }, $text: { $search: ?3 } }")
    List<RedditEntity> findBySubredditAndCreatedWithinSortByCreatedDesc(
            String subreddit, long from, long to, String keyword, Sort sort);

}
